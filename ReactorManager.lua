local requirePath = (...):match("(.-)[^%.]+$")

require(requirePath.."_libs.ReadonlyTable")

function ReactorManager(reactorPeripheral, mapping)
    local self = {
        parameters = {
                rod = {
                min = 0,
                max = 100
            },
            energy = {
                min = 0,
                max = 100
            },
        },
        current = {
            rod = 0,
            energy = 0,
        },
        reactor = reactorPeripheral,
        mapping = mapping,
        observers = {},
        running = false,
    }
    local functions;


    -- local function updateLinearMap()
    --     self.mapping = mapping(
    --         self.parameters.energy.min,
    --         self.parameters.energy.max,
    --         self.parameters.rod.min,
    --         self.parameters.rod.max
    --     )
    -- end

    -- local function getRodMin() return self.parameters.rod.min end
    -- local function setRodMin(rodMin)
    --     self.parameters.rod.min=rodMin
    --     updateLinearMap()
    --  end

    -- local function getRodMax() return self.parameters.rod.max end
    -- local function setRodMax(rodMax)
    --     self.parameters.rod.max=rodMax
    --     updateLinearMap()
    -- end

    -- local function getEnergyMin() return self.parameters.energy.min end
    -- local function setEnergyMin(energyMin)
    --     self.parameters.energy.max=energyMin
    --     updateLinearMap()
    -- end

    -- local function getEnergyMax() return self.parameters.energy.max end
    -- local function setEnergyMax(energyMax)
    --     self.parameters.energy.max=energyMax
    --     updateLinearMap()
    -- end

    
    local function getCurrentRod() return self.current.rod end
    local function getCurrentEnergy() return self.current.energy end


    local function addObserver(observerCallback)
        table.insert(self.observers, observerCallback)
    end

    local function removeObserver(observerCallback)
        for i, o in pairs(self.observers) do
            if o==observerCallback then
                table.remove(self.observers, i)
                return;
            end
        end
    end

    local function updateObservers()
        for _, observerCallback in pairs(self.observers) do
            observerCallback(functions)
        end
    end

    local function updateReactor()
        self.current.energy = self.reactor.battery().stored()*100/self.reactor.battery().capacity()
        self.current.rod = self.mapping.map(self.current.energy)

        for i = 1, self.reactor.controlRodCount() do
            self.reactor.getControlRod(i-1).setLevel(self.current.rod)
        end

        updateObservers()
    end

    local function run()
        self.running = true;
        while self.running do
            updateReactor()
            print(string.format("%.2f -> %.2f", getCurrentEnergy(), getCurrentRod()))
            sleep(0.1)
        end
    end

    local function stop()
        self.running = false
    end

    functions = readonlytable{
        -- getRodMin=getRodMin,
        -- setRodMin=setRodMin,
        -- getRodMax=getRodMax,
        -- setRodMax=setRodMax,
        -- getEnergyMin=getEnergyMin,
        -- setEnergyMin=setEnergyMin,
        -- getEnergyMax=getEnergyMax,
        -- setEnergyMax=setEnergyMax,

        getCurrentRod=getCurrentRod,
        getCurrentEnergy=getCurrentEnergy,

        addObserver=addObserver,
        removeObserver=removeObserver,

        run=run,
        stop=stop,
    }
    return functions;
end
