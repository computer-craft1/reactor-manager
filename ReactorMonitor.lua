local requirePath = (...):match("(.-)[^%.]+$")
require(requirePath.."_libs.ReadonlyTable")
local termUtils = require(requirePath.."_libs.monitorUtils")

function ReactorMonitor(monitorPeripheral, reactorManager)
    local functions;

    local MONITOR_SIZE = {w=29,h=12}

    local BG = colors.black

    local ENERGY_BAR = {
        TEXT={
            POS ={X=5,Y=3},
            COLOR=colors.white
        },
        POS={X=5,Y=4},
        SIZE = {W=4,H=9},
        COLORS = {
            BG=colors.gray,
            LEVEL=colors.red
        }
    }

    local ROD_BAR = {
        TEXT={
            POS ={X=12,Y=3},
            COLOR=colors.white
        },
        BG={
            POS = {X=12,Y=4},
            SIZE = {W=4,H=9},
            COLOR=colors.green
        },
        ROD={
            POS = {X=13,Y=4},
            WIDTH=2,
            COLOR=colors.lightGray
        }
        
    }

    local self = {
        monitor = monitorPeripheral,
        reactorManager = reactorManager,
        rodLinearMap = LinearMap(0,100, ROD_BAR.BG.POS.Y-1, ROD_BAR.BG.POS.Y+ROD_BAR.BG.SIZE.H-1),
        energyLinearMap = LinearMap(0,100, ENERGY_BAR.POS.Y+ENERGY_BAR.SIZE.H, ENERGY_BAR.POS.Y)
    }

    local function update()
        -- energy
        local energyLevelPercent = self.reactorManager.getCurrentEnergy()
        local formattedEnergyNumber = string.format("%3d", energyLevelPercent).."%"
        termUtils.writeColored(self.monitor, formattedEnergyNumber, ENERGY_BAR.TEXT.POS.X, ENERGY_BAR.TEXT.POS.Y, ENERGY_BAR.TEXT.COLOR, BG)

        local energyLevelY = math.floor(self.energyLinearMap.map(energyLevelPercent) + 0.5)
        if energyLevelY > ENERGY_BAR.POS.Y then
            termUtils.fillArea(self.monitor,
            ENERGY_BAR.POS.X, ENERGY_BAR.POS.X+ENERGY_BAR.SIZE.W-1,
            ENERGY_BAR.POS.Y, energyLevelY-1,
            ENERGY_BAR.COLORS.BG
        )
        end
        if energyLevelY < ENERGY_BAR.POS.Y+ENERGY_BAR.SIZE.H then
            termUtils.fillArea(self.monitor,
                ENERGY_BAR.POS.X, ENERGY_BAR.POS.X+ENERGY_BAR.SIZE.W-1,
                energyLevelY, ENERGY_BAR.POS.Y+ENERGY_BAR.SIZE.H-1,
                ENERGY_BAR.COLORS.LEVEL
            )
        end

        --rod
        local rodLevelPercent = self.reactorManager.getCurrentRod()
        local formattedRodNumber = string.format("%3d", rodLevelPercent).."%"
        termUtils.writeColored(self.monitor, formattedRodNumber, ROD_BAR.TEXT.POS.X, ROD_BAR.TEXT.POS.Y, ROD_BAR.TEXT.COLOR, BG)

        local rodY = math.floor(self.rodLinearMap.map(rodLevelPercent)+0.5)
        termUtils.fillArea(self.monitor,
            ROD_BAR.BG.POS.X, ROD_BAR.BG.POS.X+ROD_BAR.BG.SIZE.W-1,
            ROD_BAR.BG.POS.Y, ROD_BAR.BG.POS.Y+ROD_BAR.BG.SIZE.H-1,
            ROD_BAR.BG.COLOR
        )
        if rodY >= ROD_BAR.ROD.POS.Y then
            termUtils.fillArea(self.monitor,
                ROD_BAR.ROD.POS.X, ROD_BAR.ROD.POS.X+ROD_BAR.ROD.WIDTH-1,
                ROD_BAR.ROD.POS.Y, rodY,
                ROD_BAR.ROD.COLOR
            )
        end
    end

    local function run()
        while true do
            
        end
    end

    reactorManager.addObserver(update)
    functions = readonlytable{
        run=run
    }
    return functions
end