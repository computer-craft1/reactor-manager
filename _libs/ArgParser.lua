local requirePath = (...):match("(.-)[^%.]+$")

require(requirePath.."ReadonlyTable")

return function (args)
    local parsedArgs = {
        [""] = {}
    }
    local i=1
    local prevMarker = nil

    for _, argValue in ipairs(args) do
        if string.sub(argValue, 1, 2)=="--" then
            if prevMarker~=nil then
                table[prevMarker] = true
            end
            prevMarker = argValue
        else
            if prevMarker~=nil then
                if parsedArgs[prevMarker]==nil then
                    parsedArgs[prevMarker] = argValue
                elseif type(parsedArgs[prevMarker])=="table" then
                    table.insert(parsedArgs[prevMarker], argValue)
                else
                    parsedArgs[prevMarker] = {
                        parsedArgs[prevMarker],
                        argValue
                    }
                end
                prevMarker=nil
            else
                parsedArgs[""][i] = argValue
                i=i+1
            end
        end

    end
    return parsedArgs
end