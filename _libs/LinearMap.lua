local requirePath = (...):match("(.-)[^%.]+$")

require(requirePath.."ReadonlyTable")

function LinearMap(xa, xb, ya, yb)
    local self = {}
    self.slope = (ya-yb)/(xa-xb)
    self.offset = ya - xa*self.slope
    self.min = xa
    self.max = xb

    local function map(value)
        if value < self.min or value > self.max then
            return false
        end

        return value * self.slope + self.offset
    end

    return readonlytable{
        map = map,
        min = function () return self.min end,
        max = function () return self.max end
    }
end

