local requirePath = (...):match("(.-)[^%.]+$")

require(requirePath.."ReadonlyTable")

function MultiMap(mappings)
    table.sort(mappings, function (a, b)
        return a.min() < b.min()
    end)

    for i = 2, #mappings do
        assert(math.abs(mappings[i-1].max() - mappings[i].min()) < 0.0000001, "Linear maps do not follow each others");
    end


    local self = {
        mappings = mappings,
        min = mappings[1].min(),
        max = mappings[#mappings].max()
    }

    local function map(value)
        assert(value >= self.min and value <= self.max, "Value to map is out of range")

        for _, mapping in ipairs(self.mappings) do
            if mapping.min() <= value and value <= mapping.max() then
                return mapping.map(value)
            end
        end

        error("No mapping could be performed")
    end

    return readonlytable{
        map = map,
        min = function () return self.min end,
        max = function () return self.max end
    }
end


function Mapping(mappingFunction, min, max)
    return readonlytable{
        min=function () return min end,
        max=function () return max end,
        map=mappingFunction
    }
end