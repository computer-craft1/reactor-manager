local termUtils= {}

function termUtils.fillArea(term, x1, x2, y1, y2, color)
    if x1>x2 then
        x1,x2 = x2,x1
    end
    if y1>y2 then
        y1,y2 = y2,y1
    end

    local currentColor = term.getBackgroundColor()
    local currentX, currentY = term.getCursorPos();
    term.setBackgroundColor(color)
    
    local fillString = string.rep(" ", x2-x1+1)

    for y = y1, y2 do
        term.setCursorPos(x1, y)
        term.write(fillString)
    end

    term.setBackgroundColor(currentColor)
    term.setCursorPos(currentX, currentY)
end

function termUtils.writeColored(term, text, x, y, fg, bg)
    local currentX, currentY, currentFg, currentBg

    if x~=nil or y~=nil then
        currentX, currentY = term.getCursorPos()
        term.setCursorPos(x or currentX, y or currentY)
    end
    if fg~=nil then
        currentFg = term.getTextColor()
        term.setTextColor(fg)
    end
    if bg ~= nil then
        currentBg = term.getBackgroundColor()
        term.setBackgroundColor(bg)
    end

    term.write(text)

    if x~=nil or y~=nil then
        term.setCursorPos(currentX, currentY)
    end
    if fg~=nil then
        term.setTextColor(currentFg)
    end
    if bg ~= nil then
        term.setBackgroundColor(currentBg)
    end
end

return termUtils