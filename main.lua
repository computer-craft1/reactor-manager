require("ReactorManager")
require("ReactorMonitor")
require("_libs.LinearMap")
require("_libs.Mappings")
local arpParse = require("_libs.ArgParser")

local args = arpParse({...})

local reactorPeripheral = peripheral.wrap(args[""][1])

if args["monitor"] == nil then
    args["monitor"] = {}
elseif type(args["monitor"])=="string" then
    args["monitor"] = {args["monitor"]}
end

local monitorPeripherals = {}
for _, monitorName in ipairs(args["monitor"]) do
    table.insert(monitorPeripherals, peripheral.wrap(monitorName))
end

local mappings = MultiMap({
    Mapping(function () return 0 end, 0, 20),
    LinearMap(20,80, 0,100),
    Mapping(function () return 100 end, 80, 100),
})

local manager = ReactorManager(reactorPeripheral, mappings)
local monitors = {}
for _, monitorPeripheral in ipairs(monitorPeripherals) do
    table.insert(monitors, ReactorMonitor(monitorPeripheral, manager))
end

local function run()
    parallel.waitForAny(manager.run)
end

while true do
    local status, err = pcall(run)
    if status then
        break
    end
    
    if type(err)=="string" and string.sub(err, -29)=="Invalid multiblock controller" then
        print("Waiting for a valid reactor...")
        sleep(5)
    else
        error(error)
    end
end